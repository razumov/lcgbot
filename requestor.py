"""
Jenkins API does not use persistent HTTP connections feature of request Python module

THis class extends jenkinsapi.utils.requester.Requester class and uses requests.Session()
"""

import logging
from os.path import expanduser

import subprocess
import shlex

from jenkinsapi.utils.requester import Requester
import cernsso
from requests import HTTPError, ConnectionError


class SSORequester(Requester):
    """
    Override get_url and post_url methods that make HTTP requests and use HTTP connections pooling
    """
    _request = None
    baseurl = None

    def __init__(self, username=None, password=None, ssl_verify=True, baseurl=None):
        super(SSORequester, self).__init__(username, password, ssl_verify, None, baseurl)
        self.baseurl = baseurl

    @property
    def request(self):
        if self._request is None:
            s, err = cernsso.get_session(self.baseurl, use_cert=False)
            if s is not None:
                self._request = s
            else:
                raise RuntimeError("SSO session failed: {0}".format(err))

        return self._request

    def get_url(self, url, params=None, headers=None, allow_redirects=True, stream=True, resubmit=False):
        requestKwargs = self.get_request_dict(
            params=params,
            headers=headers,
            allow_redirects=allow_redirects)
        try:
            resp = self.request.get(self._update_url_scheme(url), **requestKwargs)
        except ConnectionError:
            return self.get_url(url, params, headers, allow_redirects, stream, resubmit)

        if '/adfs/ls' in resp.url:
            if resubmit:
                raise RuntimeError("Recreating session failed, quitting!")

            self._request.close()
            self._request = None
            logging.warning("SSO redirect encountered, recreating session")
            subprocess.check_call(shlex.split(f'kinit {self.username}@CERN.CH -k -t bot.keytab'), timeout=15)
            return self.get_url(url, params, headers, allow_redirects, stream, True)

        logging.debug('get_url <{}>: {} (redirect: {})'.format(url, resp, resp.headers.get('location', '(none)')))
        return resp

    def post_url(self, url, params=None, data=None, files=None,
                 headers=None, allow_redirects=True, **kwargs):
        resubmit = kwargs.pop('resubmit', False)

        requestKwargs = self.get_request_dict(
            params=params,
            data=data,
            files=files,
            headers=headers,
            allow_redirects=allow_redirects,
            **kwargs)
        resp = self.request.post(self._update_url_scheme(url), **requestKwargs)

        if 'adfs/ls' in resp.url or 'adfs/ls' in resp.headers.get('location', '(none)'):
            if resubmit:
                raise RuntimeError("Recreating session failed, quitting!")

            self._request.close()
            self._request = None
            logging.warning("SSO redirect encountered, recreating session")
            subprocess.check_call(shlex.split(f'kinit {self.username}@CERN.CH -k -t bot.keytab'), timeout=15)
            return self.post_url(url, params, data, files, headers, allow_redirects, resubmit=True, **kwargs)

        logging.debug('post_url <{}>: {} (redirect: {})'.format(url, resp, resp.headers.get('location', '(none)')))
        return resp

#!/usr/bin/env python3
import asyncio
import logging
import os
from typing import Iterable, Union, Optional
from html import escape

from aiohttp import ClientSession
from gidgetlab.aiohttp import GitLabBot, GitLabAPI
from gidgetlab.sansio import Event
from jenkinsapi.build import Build
from jenkinsapi.jenkins import Jenkins
from jenkinsapi.queue import QueueItem
from six import string_types

from MatrixJob import MatrixJob
from config import *
from requestor import SSORequester

# logging.getLogger().setLevel(logging.DEBUG)

os.environ.setdefault('GL_ACCESS_TOKEN', gitlab_token)
os.environ.setdefault('GL_SECRET', gitlab_secret)

bot = GitLabBot("lcgbot", url="https://gitlab.cern.ch/")
r: SSORequester
j: Jenkins
test_job: MatrixJob
latest_job: MatrixJob
logger: logging.Logger
emoji_status = {'FAILURE': u'📛', 'ABORTED': u'☠️'}
logger: logging.Logger


def botsay(text):
    return u'🤖🗩 ' + escape(text)


def setup_logging(logfile, debug, color):
    global logger
    # print("Setup logging (debug is %s)" % (debug,))
    logger = logging.getLogger()
    logger.propagate = False
    handler = logging.StreamHandler()
    if color:
        pass
        # handler.setFormatter(
        #     colorlog.ColoredFormatter(
        #         '%(asctime)s.%(msecs)03d %(log_color)s[%(name)s:%(levelname)s]%(reset)s %(message)s',
        #         datefmt='%H:%M:%S'))
    else:
        handler.setFormatter(logging.Formatter(fmt="%(asctime)s.%(msecs)03d [%(name)s:%(levelname)s] %(message)s",
                                               datefmt='%H:%M:%S'))

    file_handler = logging.FileHandler(logfile, "w")
    file_handler.setFormatter(logging.Formatter(fmt="%(asctime)s.%(msecs)03d [%(name)s:%(levelname)s] %(message)s"))

    logger.addHandler(handler)
    logger.addHandler(file_handler)

    if not debug:
        logger.setLevel(logging.INFO)
    else:
        logger.info("Debug logging is ON")
        logger.setLevel(logging.DEBUG)

    logging.getLogger("urllib3").setLevel(logging.WARNING)


async def post_comment(body, project_id, mr_id):
    if body.startswith('#'):
        body = '\n' + body
    url2 = f"/projects/{project_id}/merge_requests/{mr_id}/notes"
    async with ClientSession() as session:
        gl = GitLabAPI(session, "lcgbot", access_token=os.getenv("GL_ACCESS_TOKEN"), url='https://gitlab.cern.ch')
        res = await gl.post(url2, data={'body': botsay(body)})
        return res


async def update_comment(body, project_id, mr_id, note_id):
    url2 = f"/projects/{project_id}/merge_requests/{mr_id}/notes/{note_id}"
    async with ClientSession() as session:
        gl = GitLabAPI(session, "lcgbot", access_token=os.getenv("GL_ACCESS_TOKEN"), url='https://gitlab.cern.ch')
        await gl.put(url2, data={'body': botsay(body)})


async def add_labels(pid, iid, labels: Union[str, Iterable]):
    if isinstance(labels, string_types):
        labels = [labels]

    async with ClientSession() as session:
        gl = GitLabAPI(session, "lcgbot", access_token=os.getenv("GL_ACCESS_TOKEN"), url='https://gitlab.cern.ch')
        res = await gl.getitem(f"/projects/{pid}/merge_requests/{iid}")
        labels_ = set(res['labels'])
        labels_.update(labels)
        await gl.put(f"/projects/{pid}/merge_requests/{iid}", data={'id': pid, 'merge_request_iid': iid,
                                                                    'labels': ','.join(labels_)})


async def remove_labels(pid, iid, labels):
    if isinstance(labels, string_types):
        labels = [labels]

    async with ClientSession() as session:
        gl = GitLabAPI(session, "lcgbot", access_token=os.getenv("GL_ACCESS_TOKEN"), url='https://gitlab.cern.ch')
        res = await gl.getitem(f"/projects/{pid}/merge_requests/{iid}")
        labels_ = set(res['labels'])
        labels_.difference_update(labels)
        await gl.put(f"/projects/{pid}/merge_requests/{iid}", data={'id': pid, 'merge_request_iid': iid,
                                                                    'labels': ','.join(labels_)})


async def reset_labels(pid, iid):
    thelist = ["test-failed", "test-passed"]
    for release in ['dev3', 'dev3python2', 'dev4', 'dev4python2']:
        thelist.extend([f'latest-{release}-failed', f'latest-{release}-passed'])
        thelist.extend([f'test-{release}-failed', f'test-{release}-passed'])
    await remove_labels(pid, iid, thelist)
    await add_labels(pid, iid, "test-needed")


async def approve(pid, iid, sha):
    async with ClientSession() as session:
        gl = GitLabAPI(session, "lcgbot", access_token=os.getenv("GL_ACCESS_TOKEN"), url='https://gitlab.cern.ch')
        logger.debug("get_label start")
        res = await gl.getitem(f"/projects/{pid}/merge_requests/{iid}")
        logger.debug("get_label end")
        for release in ['dev3', 'dev3python2', 'dev4', 'dev4python2']:
            if f'latest-{release}-failed' in res['labels']:
                break
        else:
            await gl.post(f'/projects/{pid}/merge_requests/{iid}/approve', data={'id': pid, 'merge_request_iid': iid,
                                                                                 'sha': sha})
            return True

        return False


# Taken from jenkinsapi: build.block_until_complete
async def build_block_until_complete(qi: Build, delay=15) -> None:
    print("build_block_until_complete:", qi.name)
    assert isinstance(delay, int)
    while qi.is_running():
        print("still running")
        await asyncio.sleep(delay)

    print("build done")


# Taken from jenkinsapi: queue.block_until_building
async def block_until_building(qi: QueueItem, delay=5) -> Build:
    print("block_until_building: ", qi.name)
    while True:
        from jenkinsapi.custom_exceptions import NotBuiltYet
        from requests import HTTPError

        try:
            qi.poll()
            return qi.get_build()
        except (NotBuiltYet, HTTPError):
            print("not built yet")
            await asyncio.sleep(delay)
            continue


# Taken from jenkinsapi: queue.block_until_complete
async def queue_block_until_complete(qi: QueueItem, delay=5, noteid=None, pid=None, iid=None) -> Build:
    print("queue_block_until_complete: ", qi.name)
    build = await block_until_building(qi, delay)
    if noteid is not None:
        await update_comment("Build running: " + build.baseurl, pid, iid, noteid)

    await build_block_until_complete(build, delay=delay)
    print("queue_block_until_complete done")
    return build


def get_combinations(release: str) -> list:
    target_combinations = MatrixJob(name=f"lcg_ext_{release}_archdocker_nonblocking", jenkins=j).get_combinations()

    matrix = []
    for comb in target_combinations:
        matrix.append('{LABEL}/{COMPILER}/{BUILDTYPE}'.format(**comb))

    return matrix


async def run_build(job: MatrixJob, params: dict, project_id: Union[str, int], mr_id: int, config: Iterable[str],
                    release: str, build_type: str):
    params['matrixConfig'] = ' '.join(config)

    # old_combinations = job.set_combinations_fromjob(f"lcg_ext_{release}_archdocker_nonblocking")
    qi: QueueItem = job.invoke(build_params=params, cause="Merge request #" + str(mr_id), block=False)
    res = await post_comment(f'Build queued', project_id, mr_id)
    note_id = res['id']

    # job.set_combinations(old_combinations)

    build = await queue_block_until_complete(qi, noteid=note_id, pid=project_id, iid=mr_id)
    build.poll()

    payload = ["", "### Build results for job {0}, release {1}".format(job.name, release),
               "Build [#{0}]({1}) - {2}".format(build.get_number(), build.baseurl, build.get_status()), "",
               "| Platform  | Result |", "|:-----------|:-----------|"]

    build_passed = True
    logger.debug(f"Gathering results of {job.name} build #{build.get_number()}")
    for x in build.get_matrix_runs():
        logger.debug(f"* {x.name} -> {x.get_status()} -> " + emoji_status.get(x.get_status(), u'✅'))
        platform = x.name.split()[2]
        status = x.get_status()
        if 'ubuntu' not in platform and status != 'SUCCESS':
            build_passed = False

        payload.append('| ' + platform + ' | ' + emoji_status.get(status, u'✅'))

    if release != '':
        if build_passed:
            await add_labels(project_id, mr_id, f"{build_type}-{release}-passed")
        else:
            await add_labels(project_id, mr_id, f"{build_type}-{release}-failed")

    await post_comment('\n'.join(payload), project_id, mr_id)


# noinspection PyUnusedLocal
async def latest_please(event: Event, gl: GitLabAPI):
    # global j
    # pid = event.project_id
    # iid = event.data['merge_request']['iid']
    # releases = ['dev3', 'dev3python3', 'dev4']
    #
    # if not event.data['user']['username'] in can_build:
    #     await post_comment(f"User {event.data['user']['username']} is not allowed to build!", pid, iid)
    #     return
    #
    # for rl in releases:
    #     # noinspection PyDictCreation
    #     params = {'VERSION_MAIN': event.data['merge_request']['source_branch'], 'LCG_TOOLCHAIN': rl,
    #               'DEPLOY_FAILING_BUILD': 'True'}
    #     asyncio.ensure_future(run_build(latest_job, params, pid, iid, rl, 'latest'))
    raise NotImplementedError()


# noinspection PyUnusedLocal
async def test_please(event: Event, gl: GitLabAPI, *args):
    async def usage():
        await post_comment("Usage: bot test (package) in (dev3|dev3python2|dev4|dev4python2|all)+ ((also|for) <combination>+)?"
                           " please!\n\nWhere <combination> is '<label>/<compiler>/<buildtype>'", pid, iid)

    global j
    pid = event.project_id
    iid = event.data['merge_request']['iid']
    try:
        source_url = event.data['merge_request']['source']['git_http_url']
    except KeyError:
        source_url = 'https://gitlab.cern.ch/sft/lcgcmake.git'

    if len(args) < 3:
        print("Not enough arguments!")
        asyncio.ensure_future(usage())
        return

    target = args[0]

    if args[1] != 'in':
        print("Missing keyword 'in'!")
        asyncio.ensure_future(usage())
        return

    if args[1] == 'in':
        releases = set()
        extra_platforms = set()
        mode = 'DEV'
        for x in args[2:]:
            if x == 'also':
                if mode == 'DEV' or mode == 'ALSO':
                    mode = 'ALSO'
                if mode == 'ONLY':
                    print('Both <also> and <for> specified!')
                    asyncio.ensure_future(usage())
                    return

                continue

            if x == 'for':
                if mode == 'DEV' or mode == 'ONLY':
                    mode = 'ONLY'

                if mode == 'ALSO':
                    print('Both <also> and <for> specified!')
                    asyncio.ensure_future(usage())
                    return

                continue

            if x == 'all' and mode == 'DEV':
                releases = {'dev3', 'dev3python2', 'dev4', 'dev4python2'}
                continue

            if x in ('dev3', 'dev3python2', 'dev4', 'dev4python2') and mode == 'DEV':
                releases.add(x)
                continue

            if mode != 'DEV':
                extra_platforms.add(x)

        releases = list(releases)

        if not releases:
            print("Invalid release(s)")
            asyncio.ensure_future(usage())
            return

        for rl in releases:
            params = {'VERSION_MAIN': event.data['merge_request']['source_branch'], 'LCG_VERSION': rl, 'TARGET': target,
                      'VIEWS_CREATION': 'False', 'LCGCMAKE_REPO': source_url}

            if mode == 'ALSO' or mode == 'DEV':
                comb = get_combinations(rl)
                comb.extend(extra_platforms)
            else:
                comb = extra_platforms

            logger.debug(f"Combinations set to {comb}")
            asyncio.ensure_future(run_build(test_job, params, pid, iid, comb, rl, 'test'))


# noinspection PyUnusedLocal
@bot.router.register("Note Hook", noteable_type="MergeRequest")
async def mr_comment_event(event: Event, gl: GitLabAPI, *args, **kwargs):
    logger.debug("mr_comment_event start")
    # print("args", args)
    # print("kwargs", kwargs)
    pid = event.project_id
    iid = event.data['merge_request']['iid']

    message = event.object_attributes['note'].strip()
    try:
        bott, msg = message.split(' ', 1)
        mesg, please = msg.rsplit(' ', 1)
    except ValueError:
        return

    if not (bott.lower() == 'bot' and please.lower() == 'please!'):
        return

    try:
        verb, args_s = mesg.split(' ', 1)
    except ValueError:
        verb = mesg
        args_s = ""

    args = args_s.split(' ')

    if verb == 'ping':
        # print(event.data)
        res = await post_comment('Pong?', pid, iid)
        note_id = res['id']
        await asyncio.sleep(10)
        await gl.put(f"/projects/{pid}/merge_requests/{iid}/notes/{note_id}", data={'body': botsay('Pong!')})
        # print('res', res)

    if verb == 'test' or verb == 'build':
        await test_please(event, gl, *args)

    logger.debug("mr_comment_event end")


# noinspection PyUnusedLocal
@bot.router.register("Merge Request Hook", action="open")
async def mr_open_event(event: Event, gl: GitLabAPI, *args, **kwargs):
    logger.debug("mr_open_event start")
    pid = event.project_id
    iid = event.data['object_attributes']['iid']
    # await gl.put(f"/projects/{pid}/merge_requests/{iid}", data={'body': {'labels': ''}})
    asyncio.ensure_future(add_labels(pid, iid, "test-needed"))
    logger.debug("mr_open_event end")


# noinspection PyUnusedLocal
@bot.router.register("Merge Request Hook", action="update")
async def mr_update_event(event: Event, gl: GitLabAPI, *args, **kwargs):
    logger.debug("mr_update_event start")
    if 'oldrev' in event.data['object_attributes']:
        pid = event.project_id
        iid = event.data['object_attributes']['iid']
        await reset_labels(pid, iid)

    logger.debug("mr_update_event end")


def init_jenkins(baseurl, username, password):
    global r, j
    # print "Setup SSO"
    r = SSORequester(baseurl=baseurl, username=username, password=password, ssl_verify=False)
    # print "Setup Jenkins API"
    j = Jenkins(baseurl=baseurl, requester=r)


if __name__ == "__main__":
    setup_logging("bot.log", True, False)
    logger.info("Connecting to the new Jenkins...")
    init_jenkins(baseurl="https://lcgapp-services.cern.ch/spi-jenkins/", username=jenkins_username,
                 password=jenkins_password)

    if (r is None) or (j is None):
        exit(1)

    test_job = MatrixJob(name='lcg_personal_experimental_archdocker_nonblocking', jenkins=j)
    latest_job = test_job
    logger.info("Starting bot...")
    bot.run()

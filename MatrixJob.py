import xml.etree.ElementTree as ElementTree
from typing import Iterable, Union, Mapping
from collections.abc import Iterable as cIterable
from urllib.parse import urlsplit, urlunsplit

from jenkinsapi.custom_exceptions import BadParams
from jenkinsapi.job import Job
from jenkinsapi.queue import QueueItem
from six import BytesIO, iteritems


class MatrixJob(Job):
    def __init__(self, job=None, name=None, jenkins=None):
        self.old_combinations = None
        if job is not None:
            super(MatrixJob, self).__init__(job.url, job.name, job.jenkins)
        elif name is not None and jenkins is not None:
            super(MatrixJob, self).__init__(self.strip_trailing_slash(jenkins.baseurl) + '/job/' + name, name, jenkins)

    @staticmethod
    def build_condition(combinations):
        assert isinstance(combinations, list)

        results = []

        for combination in combinations:
            assert isinstance(combination, dict)
            result = " && ".join("{0} == '{1}'".format(k, v) for k, v in iteritems(combination))
            results.append('({0})'.format(result))

        return " || ".join(results)

    def _set_combinations_fromstr(self, combinations: str):
        root = self._get_config_element_tree()
        tree = ElementTree.ElementTree(root)
        filter_node = tree.find('.//defaultCombinationFilter')
        old_combinations = filter_node.text
        filter_node.text = combinations

        xml_buffer = BytesIO()
        try:
            tree.write(xml_buffer)
        except TypeError:
            raise
        self.update_config(xml_buffer.getvalue().decode("utf-8"))
        xml_buffer.close()

        return old_combinations

    def _set_combinations_fromlist(self, combinations: Iterable[Mapping[str, str]]):
        # tmp = ("&&".join(f'{k} == {v}' for k, v in x.items()) for x in combinations)
        return self._set_combinations_fromstr('(' +
                                              ") || (".join((" && ".join(f"{k} == '{v}'" for k, v in x.items()) for x in
                                                             combinations))
                                              + ')')

    def set_combinations_fromjob(self, job_name: str):
        orig = Job(self.strip_trailing_slash(self.jenkins.baseurl) + '/job/' + job_name, job_name, self.jenkins)
        root = orig._get_config_element_tree()
        tree = ElementTree.ElementTree(root)
        filter_node = tree.find('.//defaultCombinationFilter')
        comb = filter_node.text.replace('lcg_docker_', '').replace("'cc7'", "'centos7'")
        return self._set_combinations_fromstr(comb)

    def set_combinations(self, source: Union[str, Iterable[Mapping[str, str]], Job]) -> str:
        if isinstance(source, str):
            return self._set_combinations_fromstr(source)
        elif isinstance(source, cIterable):
            return self._set_combinations_fromlist(source)
        else:
            return self.get_combinations_str()

    def get_combinations_str(self):
        root = self._get_config_element_tree()
        tree = ElementTree.ElementTree(root)
        filter_node = tree.find('.//defaultCombinationFilter')
        combinations_text = filter_node.text
        return combinations_text

    def get_combinations(self) -> Iterable[Mapping[str, str]]:
        combinations_text = self.get_combinations_str().translate(str.maketrans({'(': None, ')': None, "'": None}))
        combinations_list = list(x.strip() for x in combinations_text.split('||'))
        combinations_list_2 = list((y.strip() for y in x.split('&&')) for x in combinations_list)
        # noinspection PyTypeChecker
        combinations_list_dict = list(
            dict((z.strip() for z in y.split('==')) for y in x) for x in combinations_list_2)

        return combinations_list_dict

    @staticmethod
    def filter_combinations(combinations, k, v):
        return [x for x in combinations if x[k] == v]

    # def _get_config_element_tree_from(self, job_name):
    #     orig = Job(self.strip_trailing_slash(self.jenkins.baseurl) + '/job/' + job_name, job_name, self.jenkins)
    #     return orig._get_config_element_tree()
    #
    # @staticmethod
    # def _set_combinations(root, combinations):
    #     filter_node = root.find('.//defaultCombinationFilter')
    #     old_combinations = filter_node.text
    #     filter_node.text = combinations
    #     return old_combinations
    #
    # def get_new_combinations(self, job_name):
    #     root = self._get_config_element_tree_from(job_name)
    #     filter_node = root.find('.//defaultCombinationFilter')
    #     combinations = filter_node.text.replace('lcg_docker_slc6', 'slc6').replace('lcg_docker_cc7', 'centos7')
    #     return combinations
    #
    # def _update_combinations(self, job_name):
    #     root = self._get_config_element_tree()
    #     tree = ElementTree.ElementTree(root)
    #     combinations = self.get_new_combinations(job_name)
    #     self.old_combinations = self._set_combinations(tree, combinations)
    #
    #     xml_buffer = BytesIO()
    #     try:
    #         tree.write(xml_buffer)
    #     except TypeError:
    #         raise
    #     self.update_config(xml_buffer.getvalue().decode("utf-8"))
    #     xml_buffer.close()
    #
    # def _reset_combinations(self):
    #     root = self._get_config_element_tree()
    #     tree = ElementTree.ElementTree(root)
    #     self._set_combinations(tree, self.old_combinations)
    #
    #     xml_buffer = BytesIO()
    #     tree.write(xml_buffer)
    #     self.update_config(xml_buffer.getvalue().decode("utf-8"))
    #     xml_buffer.close()

    # def invoke(self, securitytoken=None, block=False, build_params=None, cause=None, files=None, delay=5):
    #     res = super(MatrixJob, self).invoke(securitytoken=securitytoken, block=block,
    #                                         build_params=build_params, cause=cause,
    #                                         files=files, delay=delay)
    #     return res

    def invoke(self, securitytoken=None, block=False,
               build_params=None, cause=None, files=None, delay=5):
        assert isinstance(block, bool)
        if build_params and (not self.has_params()):
            raise BadParams("This job does not support parameters")

        params = {}  # Via Get string

        if securitytoken:
            params['token'] = securitytoken

        # Either copy the params dict or make a new one.
        build_params = dict(build_params.items()) \
            if build_params else {}  # Via POSTed JSON

        url = self.get_build_triggerurl()
        if cause:
            build_params['cause'] = cause

        # Build require params as form fields
        # and as Json.
        data = {
            'json': self.mk_json_from_build_parameters(
                build_params,
                files)
        }
        data.update(build_params)

        response = self.jenkins.requester.post_and_confirm_status(
            url,
            data=data,
            params=params,
            files=files,
            valid=[200, 201, 303],
            allow_redirects=False
        )

        redirect_url = response.headers['location']

        netloc, path, query, fragment = urlsplit(redirect_url)[1:]  # Remove scheme and fragment
        scheme = urlsplit(self.jenkins.baseurl)[0]
        redirect_url = urlunsplit((scheme, netloc, path, query, fragment))

        if not redirect_url.startswith("%s/queue/item" % self.jenkins.baseurl):
            raise ValueError("Not a Queue URL: %s" % redirect_url)

        qi = QueueItem(redirect_url, self.jenkins)
        if block:
            qi.block_until_complete(delay=delay)
        return qi

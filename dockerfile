FROM python:3

ARG DEBIAN_FRONTEND=noninteractive

WORKDIR /usr/src/app

EXPOSE 8080/tcp

RUN useradd -s /bin/bash lcgbot && chown lcgbot:lcgbot /usr/src/app

RUN apt-get update && apt-get install -yq krb5-user && wget https://gitlab.cern.ch/sft/docker/raw/master/misc/krb5.conf -O /etc/krb5.conf

COPY requirements.txt /tmp

RUN pip install --no-cache-dir -r /tmp/requirements.txt

USER lcgbot

COPY --chown=lcgbot:lcgbot bot.keytab  bot.py  cernsso.py  config.py  job.py  MatrixJob.py  requestor.py requirements.txt  ./

CMD kinit -kt bot.keytab lcgbot@CERN.CH && python ./bot.py

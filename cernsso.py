import ssl
from collections import defaultdict
from html.parser import HTMLParser
from typing import Tuple, Optional

import requests
import urllib3
from requests_kerberos import HTTPKerberosAuth, OPTIONAL
# import logging

from config import jenkins_username as kerberos_username

kerberos_username = kerberos_username + '@CERN.CH'

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# logging.basicConfig(level=logging.DEBUG)


class FormParser(HTMLParser):
    def error(self, message):
        pass

    def __init__(self):
        HTMLParser.__init__(self)
        self.action = ""
        self.fields = {}

    def handle_starttag(self, tag, attrs):
        attrs_d = defaultdict(default_factory=lambda: "")
        attrs_d.update(dict(attrs))
        if tag == "form":
            self.action = attrs_d["action"]
        elif tag == "input":
            if attrs_d["type"] != "submit":
                self.fields[attrs_d["name"]] = attrs_d["value"]
            else:
                self.fields["submit"] = attrs_d["value"]


VERSION = '0.6'

CERN_SSO_CURL_USER_AGENT_KRB = 'curl-sso-kerberos/' + VERSION + ' (Mozilla)'
CERN_SSO_CURL_USER_AGENT_CERT = 'curl-sso-certificate/' + VERSION + '(Mozilla)'
CERN_SSO_CURL_ADFS_EP = '/adfs/ls'
CERN_SSO_CURL_ADFS_SIGNIN = 'wa=wsignin1.0'
CERN_SSO_CURL_AUTHERR = 'HTTP Error 401.2 - Unauthorized'
CERN_SSO_CURL_CAPATH = '/etc/ssl/certs'


def get_session(url, cert=None, key=None, use_cert=False) -> Tuple[Optional[requests.Session], Optional[str]]:
    if use_cert:
        ssl_ctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
        ssl_ctx.load_cert_chain(certfile=cert, keyfile=key)

        s = requests.session()
        s.headers = {'User-Agent': CERN_SSO_CURL_USER_AGENT_CERT}
        s.cert = (cert, key)
    else:
        s = requests.Session()
        s.headers = {'User-Agent': CERN_SSO_CURL_USER_AGENT_KRB}
        s.auth = HTTPKerberosAuth(mutual_authentication=OPTIONAL, delegate=True, principal=kerberos_username)

    #    r = s.get(url, allow_redirects=True, verify='./CERN Root Certification Authority 2.crt')
    r = s.get(url, allow_redirects=True, verify=False)
    if r.status_code != requests.codes.ok:
        return None, "Bad status code from get {1}: {0}".format(r.status_code, url)  # + r.text

    if CERN_SSO_CURL_ADFS_EP not in r.url:
        return s, "SSO not detected"

    parser = FormParser()
    parser.feed(r.text)

    r = s.post(parser.action, parser.fields)
    if r.status_code != requests.codes.ok:
        return None, "Bad status code from post {1}: {0}".format(r.status_code, parser.action)

    if CERN_SSO_CURL_AUTHERR in r.text:
        return None, "Authentication error from post {1}: {0}".format(r.status_code, parser.action)

    if CERN_SSO_CURL_ADFS_SIGNIN in r.url:
        return None, "Bad redirect after post: {0}".format(r.url)

    return s, r.status_code


if __name__ == "__main__":
    sess, err = get_session("https://gitlab.cern.ch", use_cert=False)
    if sess is not None:
        resp = sess.get("https://gitlab.cern.ch/projects/44696")
        resp.raise_for_status()
        print(resp)
    else:
        print("Error:", err)
